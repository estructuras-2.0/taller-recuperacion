import sys
import networkx as nx
import matplotlib.pyplot as plt
import heapq

def menuPila():
    stack = []
    while True:
        print("\nSubmenú de Pilas")
        print("1. Apilar")
        print("2. Desapilar")
        print("3. Mostrar pila")
        print("0. Volver al menú principal")
        opcion = input("Seleccione una opción: ")

        if opcion == '1':
            elemento = input("Ingrese el elemento a apilar: ")
            stack.append(elemento)
        elif opcion == '2':
            if stack:
                print("Elemento desapilado:", stack.pop())
            else:
                print("La pila está vacía.")
        elif opcion == '3':
            print("Pila:", stack)
        elif opcion == '0':
            break
        else:
            print("Opción no válida. Intente nuevamente.")

def menuColas():
    queue = []
    while True:
        print("\nSubmenú de Colas")
        print("1. Encolar")
        print("2. Desencolar")
        print("3. Mostrar cola")
        print("0. Volver al menú principal")
        opcion = input("Digite una opción: ")

        if opcion == '1':
            elemento = input("Ingrese el elemento a encolar: ")
            queue.append(elemento)
        elif opcion == '2':
            if queue:
                print("Elemento desencolado:", queue.pop(0))
            else:
                print("La cola está vacía.")
        elif opcion == '3':
            print("Cola:", queue)
        elif opcion == '0':
            break
        else:
            print("Opción no válida. Intente nuevamente.")
class TreeNode:
    def __init__(self, llave):
        self.left = None
        self.right = None
        self.val = llave

def insertar(raiz, llave):
    if raiz is None:
        return TreeNode(llave)
    else:
        if raiz.val < llave:
            raiz.right = insertar(raiz.right, llave)
        else:
            raiz.left = insertar(raiz.left, llave)
    return raiz


def eliminarNodo(raiz, llave):
    if raiz is None:
        return raiz
    if llave < raiz.val:
        raiz.left = eliminarNodo(raiz.left, llave)
    elif llave > raiz.val:
        raiz.right = eliminarNodo(raiz.right, llave)
    else:
        # Nodo con un solo hijo o sin hijos
        if raiz.left is None:
            return raiz.right
        elif raiz.right is None:
            return raiz.left

        temp = minimoNodo(raiz.right)
        raiz.val = temp.val
        raiz.right = eliminarNodo(raiz.right, temp.val)

    return raiz

def minimoNodo(nodo):
    current = nodo
    while current.left is not None:
        current = current.left
    return current

def inorder(raiz):
    if raiz:
        inorder(raiz.left)
        print(raiz.val, end=" ")
        inorder(raiz.right)

def preorder(raiz):
    if raiz:
        print(raiz.val, end=" ")
        preorder(raiz.left)
        preorder(raiz.right)

def postorder(raiz):
    if raiz:
        postorder(raiz.left)
        postorder(raiz.right)
        print(raiz.val, end=" ")

def dibujarArbol(raiz, G=None, pos=None, x=0, y=0, layer=1):
    if G is None:
        G = nx.DiGraph()
        pos = {}
    if raiz:
        G.add_node(raiz.val)
        pos[raiz.val] = (x, y)
        if raiz.left:
            G.add_edge(raiz.val, raiz.left.val)
            l = x - 1 / layer
            dibujarArbol(raiz.left, G, pos, x=l, y=y-1, layer=layer+1)
        if raiz.right:
            G.add_edge(raiz.val, raiz.right.val)
            r = x + 1 / layer
            dibujarArbol(raiz.right, G, pos, x=r, y=y-1, layer=layer+1)
    return G, pos

def menuBinario():
    raiz = None
    while True:
        print("\nSubmenú de Árboles Binarios")
        print("1. Insertar clave")
        print("2. Mostrar recorrido inorden")
        print("3. Mostrar recorrido preorden")
        print("4. Mostrar recorrido postorden")
        print("5. Dibujar arbol")
        print("6. Eliminar clave")
        print("0. Volver al menú principal")
        opcion = input("Seleccione una opción: ")

        if opcion == '1':
            llave = int(input("Ingrese la clave a insertar: "))
            raiz = insertar(raiz, llave)
        elif opcion == '2':
            print("Recorrido inorden:")
            inorder(raiz)
            print()
        elif opcion == '3':
            print("Recorrido preorden:")
            preorder(raiz)
            print()
        elif opcion == '4':
            print("Recorrido postorden:")
            postorder(raiz)
            print()
        elif opcion == '5':
            G, pos = dibujarArbol(raiz)
            nx.draw(G, pos, with_labels=True, arrows=False)
            plt.show()
        elif opcion == '6':
            print("Eliminar Clave")
            llave = int(input("Ingrese la clave a eliminar: "))
            raiz = eliminarNodo(raiz, llave)
        elif opcion == '0':
            break
        else:
            print("Opción no válida. Intente nuevamente.")



def dijkstra(grafo, inicio, final=None):
    distancia = {nodo: float('infinity') for nodo in grafo}
    distancia[inicio] = 0
    prioridad = [(0, inicio)]
    camino = {inicio: None}

    while prioridad:
        (costo, nodo) = heapq.heappop(prioridad)

        if nodo == final:
            break

        for vecino, datos in grafo[nodo].items():
            peso = datos['weight']
            nuevo_costo = costo + peso
            if nuevo_costo < distancia[vecino]:
                distancia[vecino] = nuevo_costo
                camino[vecino] = nodo
                heapq.heappush(prioridad, (nuevo_costo, vecino))

    if final:
        if distancia[final] == float('infinity'):
            return None, None  # No hay camino
        else:
            ruta = []
            while final:
                ruta.append(final)
                final = camino[final]
            ruta.reverse()
            return distancia, ruta
    else:
        return distancia, camino


def bfs(grafo, inicio):
    visitados = []
    cola = [inicio]

    while cola:
        nodo = cola.pop(0)
        if nodo not in visitados:
            visitados.append(nodo)
            cola.extend(set(grafo[nodo].keys()) - set(visitados))

    return visitados


def dfs(grafo, inicio, visitados=None):
    if visitados is None:
        visitados = []

    visitados.append(inicio)

    for vecino in grafo[inicio].keys():
        if vecino not in visitados:
            dfs(grafo, vecino, visitados)

    return visitados
def menuGrafos():
    G = nx.Graph()
    while True:
        print("\nSubmenú de Grafos")
        print("1. Agregar nodo")
        print("2. Agregar arista")
        print("3. Eliminar nodo")
        print("4. Eliminar arista")
        print("5. Mostrar grafo")
        print("6. Dijkstra")
        print("7. BFS")
        print("8. DFS")
        print("0. Volver al menú principal")
        opcion = input("Digite una opción: ")

        if opcion == '1':
            node = input("Ingrese el nodo a agregar: ")
            G.add_node(node)
            print(f"Nodo {node} agregado.")
        elif opcion == '2':
            u = input("Ingrese el nodo de origen: ")
            v = input("Ingrese el nodo de destino: ")
            weight = int(input("Ingrese el peso de la arista: "))
            G.add_edge(u, v, weight=weight)
            print(f"Arista ({u}, {v}) con peso {weight} agregada.")
        elif opcion == '3':
            node = input("Ingrese el nodo a eliminar: ")
            if node in G:
                G.remove_node(node)
                print(f"Nodo {node} eliminado.")
            else:
                print(f"Nodo {node} no encontrado.")
        elif opcion == '4':
            u = input("Ingrese el nodo de origen: ")
            v = input("Ingrese el nodo de destino: ")
            if G.has_edge(u, v):
                G.remove_edge(u, v)
                print(f"Arista ({u}, {v}) eliminada.")
            else:
                print(f"No existe una arista entre {u} y {v}.")
        elif opcion == '5':
            pos = nx.spring_layout(G)
            labels = nx.get_edge_attributes(G, 'weight')
            nx.draw(G, pos, with_labels=True)
            nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
            plt.show()
        elif opcion == '6':
            source = input("Ingrese el nodo de inicio para Dijkstra: ")
            target = input("Ingrese el nodo de destino (o deje en blanco para todos los nodos): ")
            if source in G.nodes:
                graph_dict = {node: dict(G[node]) for node in G.nodes}
                if target and target in G.nodes:
                    dist, path = dijkstra(graph_dict, source, target)
                    if path is None:
                        print(f"No hay camino desde {source} hasta {target}.")
                    else:
                        print(f"Distancia desde {source} hasta {target}: {dist[target]}")
                        print(f"Camino: {' -> '.join(path)}")
                else:
                    dist, paths = dijkstra(graph_dict, source)
                    print(f"Distancias desde {source}: {dist}")
                    print(f"Caminos desde {source}: {paths}")
            else:
                print("Nodo no encontrado en el grafo.")
        elif opcion == '7':
            start = input("Ingrese el nodo de inicio para BFS: ")
            if start in G.nodes:
                graph_dict = {node: dict(G[node]) for node in G.nodes}
                visitados = bfs(graph_dict, start)
                print(f"Recorrido BFS desde {start}: {visitados}")
            else:
                print("Nodo no encontrado en el grafo.")
        elif opcion == '8':
            start = input("Ingrese el nodo de inicio para DFS: ")
            if start in G.nodes:
                graph_dict = {node: dict(G[node]) for node in G.nodes}
                visitados = dfs(graph_dict, start)
                print(f"Recorrido DFS desde {start}: {visitados}")
            else:
                print("Nodo no encontrado en el grafo.")
        elif opcion == '0':
            break
        else:
            print("Opción no válida. Intente nuevamente.")


class BPlusTreeNode:
    def __init__(self, t, leaf=False):
        self.t = t
        self.leaf = leaf
        self.keys = []
        self.children = []

class BPlusTree:
    def __init__(self, t):
        self.t = t
        self.root = BPlusTreeNode(t, True)

    def insert(self, llave):
        raiz = self.root
        if len(raiz.keys) == 2 * self.t - 1:
            temp = BPlusTreeNode(self.t)
            temp.children.append(self.root)
            self.split_child(temp, 0)
            self.root = temp
        self.insert_non_full(self.root, llave)

    def split_child(self, parent, i):
        t = self.t
        node = parent.children[i]
        new_node = BPlusTreeNode(t, node.leaf)
        parent.children.insert(i + 1, new_node)
        parent.keys.insert(i, node.keys[t - 1])

        new_node.keys = node.keys[t:(2 * t - 1)]
        node.keys = node.keys[0:(t - 1)]

        if not node.leaf:
            new_node.children = node.children[t:(2 * t)]
            node.children = node.children[0:t]

    def insert_non_full(self, node, key):
        if node.leaf:
            node.keys.append(key)
            node.keys.sort()
        else:
            i = len(node.keys) - 1
            while i >= 0 and key < node.keys[i]:
                i -= 1
            i += 1
            if len(node.children[i].keys) == 2 * self.t - 1:
                self.split_child(node, i)
                if key > node.keys[i]:
                    i += 1
            self.insert_non_full(node.children[i], key)

    def delete(self, key):
        self.delete_internal(self.root, key)
        if len(self.root.keys) == 0 and not self.root.leaf:
            self.root = self.root.children[0]

    def delete_internal(self, node, key):
        t = self.t
        if node.leaf:
            if key in node.keys:
                node.keys.remove(key)
                return
        else:
            for i, item in enumerate(node.keys):
                if key < item:
                    self.delete_internal(node.children[i], key)
                    return
                elif key == item:
                    self.delete_internal(node.children[i + 1], key)
                    return
            self.delete_internal(node.children[-1], key)

    def show(self, node=None, level=0):
        if node is None:
            node = self.root
        print("Level", level, "Keys:", node.keys)
        if not node.leaf:
            for child in node.children:
                self.show(child, level + 1)


def menuBPlus():
    orden = int(input("Ingrese el orden del árbol B+: "))
    arbol_bplus = BPlusTree(orden)
    while True:
        print("\nSubmenú de Árboles B+")
        print("1. Insertar clave")
        print("2. Eliminar clave")
        print("3. Ver árbol")
        print("0. Volver al menú principal")
        opcion = input("Digite una opción: ")

        if opcion == '1':
            llave = int(input("Ingrese la clave a insertar: "))
            arbol_bplus.insert(llave)
        elif opcion == '2':
            llave = int(input("Ingrese la clave a eliminar: "))
            arbol_bplus.delete(llave)
        elif opcion == '3':
            arbol_bplus.show()
        elif opcion == '0':
            break
        else:
            print("Opción no válida. Intente nuevamente.")





def main_menu():
    while True:
        print("\nMenú Principal")
        print("1. Grafos")
        print("2. Árboles Binarios")
        print("3. Árboles B+")
        print("4. Pilas")
        print("5. Colas")
        print("0. Salir")
        opcion = input("Digite una opción: ")

        if opcion == '1':
            menuGrafos()
        elif opcion == '2':
            menuBinario()
        elif opcion == '3':
            menuBPlus()
        elif opcion == '4':
            menuPila()
        elif opcion == '5':
            menuColas()
        elif opcion == '0':
            print("Saliendo...")
            sys.exit()
        else:
            print("Opción no válida. Intente nuevamente.")
if __name__ == "__main__":
    main_menu()